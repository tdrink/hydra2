package edu.wisc.ssec.adapter;
                                                                                                                                                           
import java.util.HashMap;

import visad.FlatField;

public class CrIS_SDR_Spectrum extends SpectrumAdapter {

  public static int[] ifov_order = new int[] {8,5,2,7,4,1,6,3,0};

  public HashMap new_subset = new HashMap();
  
  private float initialSpectralResolution = 0f;
  private float spectralIncrement = 0f;

  public CrIS_SDR_Spectrum(MultiDimensionReader reader, HashMap metadata) {
    super(reader, metadata);
  }

  /**
   * A valid CrIS product/variable name will always end with two capital
   * letters for spectral range, LW: longwave, MW: mediumwave, SW: shortwave
   * @param productName
   * @return
   */
  
  public int computeNumChannels() {
	  
     String arrayName = (String) metadata.get("array_name");
     int numChannels = CrIS_SDR_Utility.getNumChannels(arrayName);
     initialSpectralResolution = CrIS_SDR_Utility.getWavenumberStart(arrayName);
     spectralIncrement = CrIS_SDR_Utility.getWavenumberIncrement(arrayName);
	  
     return numChannels;
  }

  public float[] getChannels() throws Exception {
    float[] spectrum = new float[numChannels];
    for (int k=0; k < numChannels; k++) {
       spectrum[k] = initialSpectralResolution + k * (spectralIncrement);
    }
    return spectrum;
  }
  
  public float getInitialWavenumber() {
    return CrIS_SDR_Utility.getWavenumberStart(getArrayName());
  }

}
